<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/timecircles.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'days' => 'Jours',

	// H
	'hours' => 'Heures',

	// M
	'minutes' => 'Minutes',

	// S
	'seconds' => 'Secondes',

	// Y
	'years' => 'Années'
);
